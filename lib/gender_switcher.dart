library gender_switcher;

import 'package:flutter/material.dart';

@Deprecated("use [MyToggleButton] from my_utils package")
class GenderSwitcher extends StatefulWidget {

  static const _FEMALE_ICONS = "💇🏽‍♀️";
  static const _MALE_ICONS = "🙍🏽‍♂️";

  final bool isFemale;
  final ValueChanged<bool> onChanged;
  final double width;
  final double height;
  final Color backgroundColor;
  final Color selectedBackground;
  final EdgeInsets padding;
  final BorderRadius borderRadius;
  final String femaleLabel;
  final String maleLabel;
  final TextStyle? textStyle;

  GenderSwitcher({
    required this.isFemale,
    required this.onChanged,
    this.width = 80.0,
    this.height = 40.0,
    this.backgroundColor = Colors.black12,
    this.selectedBackground = Colors.white,
    this.padding = const EdgeInsets.all(2.0),
    this.borderRadius = const BorderRadius.all(Radius.circular(4.0)),
    this.femaleLabel = _FEMALE_ICONS,
    this.maleLabel = _MALE_ICONS,
    this.textStyle
  });

  @override
  _GenderSwitcherState createState() => _GenderSwitcherState();
}

class _GenderSwitcherState extends State<GenderSwitcher> {

  late bool _isFemale;

  @override
  void initState() {
    super.initState();
    _isFemale = widget.isFemale;
  }

  void _change() {
    this.setState(() { _isFemale = !_isFemale; });
    widget.onChanged(_isFemale);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _change,
      child: Container(
        width: widget.width,
        height: widget.height,
        padding: widget.padding,
        decoration: BoxDecoration(
          color: widget.backgroundColor,
          borderRadius: widget.borderRadius
        ),
        child: Stack(
          children: [

            AnimatedAlign(
              alignment: _isFemale ? Alignment.topLeft : Alignment.topRight,
              duration: const Duration(milliseconds: 100),
              curve: Curves.easeOut,
              child: Container(
                width: widget.width / 2,
                decoration: BoxDecoration(
                  color: widget.selectedBackground,
                  borderRadius: widget.borderRadius
                ),
              )
            ),

            Row(
              children: [
                _iconWidget(widget.femaleLabel),
                _iconWidget(widget.maleLabel)
              ],
            )

          ],
        ),
      ),
    );
  }

  Widget _iconWidget(String text) => Expanded(child: Center(child: Text(text, style: widget.textStyle)));

}
